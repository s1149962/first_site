<?php
//  GET
$router->get('', 'controllers/index.php');
$router->get('contact', 'controllers/contact.php');
$router->get('about', 'controllers/about.php');
$router->get('login', 'controllers/login.php');
$router->get('register', 'controllers/register.php');
$router->get('logout', 'controllers/logout.php');
$router->get('product', 'controllers/product.php');
$router->get('logged', 'controllers/logged.php');
$router->get('admin', 'controllers/admin.php');
$router->get('profile', 'controllers/profile.php');
$router->get('profiles', 'controllers/profiles.php');
$router->get('upload', 'controllers/upload.php');
$router->get('orders', 'controllers/orders.php');
$router->get('delproduct', 'controllers/delproduct.php');






// POST
$router->post('', 'controllers/index.php');
$router->post('contact', 'controllers/contact.php');
$router->post('about', 'controllers/about.php');
$router->post('login', 'controllers/login.php');
$router->post('register', 'controllers/register.php');
$router->post('logout', 'controllers/logout.php');
$router->post('product', 'controllers/product.php');
$router->post('logged', 'controllers/logged.php');
$router->post('admin', 'controllers/admin.php');
$router->post('profile', 'controllers/profile.php');
$router->post('profiles', 'controllers/profiles.php');
$router->post('upload', 'controllers/upload.php');
$router->post('orders', 'controllers/orders.php');
$router->post('delproduct', 'controllers/delproduct.php');
