<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Check' => $baseDir . '/controllers/check.php',
    'ComposerAutoloaderInitae2e034ce6e439ab137d023298ed97c1' => $vendorDir . '/composer/autoload_real.php',
    'Composer\\Autoload\\ClassLoader' => $vendorDir . '/composer/ClassLoader.php',
    'Composer\\Autoload\\ComposerStaticInitae2e034ce6e439ab137d023298ed97c1' => $vendorDir . '/composer/autoload_static.php',
    'Connection' => $baseDir . '/core/database/Connection.php',
    'QueryMyDatabase' => $baseDir . '/core/database/QueryMyDatabase.php',
    'Request' => $baseDir . '/core/Request.php',
    'Router' => $baseDir . '/core/Router.php',
);
