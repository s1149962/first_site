<?php
ini_set( 'display_errors', 1);
ini_set( 'display_startup_errors', 1);
error_reporting( E_ALL);

require 'vendor/autoload.php';
$query = require 'core/bootstrap.php';


$router = new Router;

$uri = trim($_SERVER['REQUEST_URI'], '/');

session_start();

require Router::load('routes.php')
    ->direct(Request::uri(), Request::method());

