<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Untitled (Backup 1573215491505)</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700">
    <link rel="stylesheet" href="../assets/fonts/font-awesome.min.css">
</head>

<body>
<nav class="navbar navbar-dark navbar-expand-lg fixed-top bg-dark" id="mainNav">
    <div class="container"><a class="navbar-brand" href="?uri=">name</a><button data-toggle="collapse" data-target="#navbarResponsive" class="navbar-toggler navbar-toggler-right" type="button" data-toogle="collapse" aria-controls="navbarResponsive" aria-expanded="false"
                                                                                    aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="nav navbar-nav ml-auto text-uppercase">

                <?php
                if (isset($_SESSION['admin'])) { ?>
                <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="?uri=admin">Admin</a></li>
                <?php
                }

                if (isset($_SESSION['user']) || isset($_SESSION['admin'])) { ?>
                    <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="?uri=profile">profile</a></li>
                <?php
                } ?>


                <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="?uri=#portfolio">Products</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="?uri=contact">contact us</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="?uri=about">About</a></li>


                <?php
                if (isset($_SESSION['user']) || isset($_SESSION['admin'])) {
                }
                else{ ?>
                <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="?uri=login">login</a>
                </li>
                <?php
                }
                if (isset($_SESSION['user']) || isset($_SESSION['admin'])) { ?>
                    <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="?uri=logout">logout</a>
                    </li>
                    <?php
                } ?>



            </ul>
        </div><a class="js-scroll-trigger" href="#contact">CART</a></div>
</nav>
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="../assets/js/agency.js"></script>
</body>

