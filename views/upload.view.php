<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Features - Brand</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="assets/fonts/simple-line-icons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="assets/css/smoothproducts.css">
    <link rel="stylesheet" href="css/upload.css">
</head>

<body>
<?php
require 'partials/nav.php';
?>
<section>
    <div class="container p-5">
        <div class="row" >
            <div class="col-lg-5 mx-auto">
                <div class="p-5 bg-white shadow rounded-lg"><img src="https://res.cloudinary.com/mhmd/image/upload/v1557366994/img_epm3iz.png" alt="" width="200" class="d-block mx-auto mb-4 rounded-pill">

                    <form method="post" action="?uri=upload">

                    <div class="custom-file overflow-hidden rounded-pill mb-5" >
                    <label style="color:red;" for="fileUpload" class="file-upload btn btn-primary btn-block rounded-pill shadow"><i class="fa fa-upload mr-2"></i>CHOOSE A FILE ...
                        <input id="fileUpload" type="file" name="picture">
                    </label>
                </div>
                    <label class="file-upload btn btn-primary btn-block rounded-pill shadow">
                        <input id="fileUpload" type="text" name="name" placeholder="Product name:">
                    </label>
                    <label class="file-upload btn btn-primary btn-block rounded-pill shadow">
                        <input id="fileUpload" type="text" name="description" placeholder="Product description:">
                    </label>
                    <label class="file-upload btn btn-primary btn-block rounded-pill shadow">
                        <input id="fileUpload" type="text" name="extra" placeholder="extra description: (optional)">
                    </label>
                    <label class="file-upload btn btn-primary btn-block rounded-pill shadow">
                        <input id="fileUpload" type="text" name="amount" placeholder="Product quantity:">
                    </label>
                    <label class="file-upload btn btn-primary btn-block rounded-pill shadow">
                        <input id="fileUpload" type="text" name="price" placeholder="Product Price:">
                    </label>

                    <center>
                        <p class="file-upload btn btn-primary btn-block rounded-pill shadow" style="color: white; background-color: red;" >
                        <?php
                        echo $message;
                        ?>
                        </p>
                        <input id="fileUpload" class="file-upload btn btn-primary btn-block rounded-pill shadow" type="submit" name="upload">
                    </center>

                    </form>
            </div>
        </div>
    </div>
</section>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="assets/js/smoothproducts.min.js"></script>
<script src="assets/js/theme.js"></script>
</body>

</html>
