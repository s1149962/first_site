<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>HomePage</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700">
    <link rel="stylesheet" href="../assets/fonts/font-awesome.min.css">

</head>
<?php require 'partials/nav.php'; ?>
<body id="page-top">
<br/>

<section class="bg-light">
    <div class="container" id="portfolio">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="text-uppercase section-heading">Admin panel</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4 portfolio-item">
                <a class="portfolio-link"  href="?uri=orders">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content"><i class="fa fa-plus fa-3x"></i></div>
                    </div><img style="height: 262px; width: 350px;" class="img-fluid" src="images/order.jpg"></a>
                <div class="portfolio-caption">
                    <h4>orders list</h4>
                    <p class="text-muted">all orders</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 portfolio-item">
                <a class="portfolio-link"  href="?uri=upload">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content"><i class="fa fa-plus fa-3x"></i></div>
                    </div><img style="height: 262px; width: 350px;" class="img-fluid" src="images/add.jpg"></a>
                <div class="portfolio-caption">
                    <h4>Product upload</h4>
                    <p class="text-muted">Page for uploading products</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 portfolio-item">
                <a class="portfolio-link"  href="?uri=profile">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content"><i class="fa fa-plus fa-3x"></i></div>
                    </div><img style="height: 262px; width: 350px;" class="img-fluid" src="images/users.jpg"></a>
                <div class="portfolio-caption">
                    <h4>Profiles page</h4>
                    <p class="text-muted">Page with all user profiles</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 portfolio-item">
                <a class="portfolio-link"  href="?uri=admin">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content"><i class="fa fa-plus fa-3x"></i></div>
                    </div><img class="img-fluid" src="images/x.png"></a>
                <div class="portfolio-caption">
                    <h4>Coming soon</h4>
                    <p class="text-muted">for future expansion</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 portfolio-item">
                <a class="portfolio-link"  href="?uri=product">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content"><i class="fa fa-plus fa-3x"></i></div>
                    </div><img style="height: 262px; width: 350px;" class="img-fluid img-fluid" src="images/delproduct.png"></a>
                <div class="portfolio-caption">
                    <h4>Product deletion page</h4>
                    <p class="text-muted">Delete or edit a existing product</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 portfolio-item">
                <a class="portfolio-link"  href="?uri=admin">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content"><i class="fa fa-plus fa-3x"></i></div>
                    </div><img class="img-fluid" src="images/x.png"></a>
                <div class="portfolio-caption">
                    <h4>Comming soon</h4>
                    <p class="text-muted">for future expansion</p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require 'partials/footer.php'; ?>
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="../assets/js/agency.js"></script>
</body>

</html>