<?php
$app = [];
$app['config'] = require 'core/config.php';

$app['database'] = new QueryMyDatabase(
    Connection::make($app['config']['database'])
);
$conn = $app['database'];